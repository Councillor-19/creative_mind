import React from "react"
import { graphql } from "gatsby"
import Layout from '../../components/layout'
import { Heading } from '@chakra-ui/core'
import BackgroundImage from 'gatsby-background-image'
import Img from "gatsby-image"

import './home.scss'

export default ({ data }) => {
  const { page } = data
  const { title, content, featuredImage, homepage_quote } = page

  return (
    <Layout>
      {!!featuredImage?.node?.remoteFile?.childImageSharp && (
        <BackgroundImage
          Tag="div"
          className="homepage-background"
          fluid={featuredImage.node.remoteFile.childImageSharp.fluid} >
          <div className="container">
            <div className="homepage-background__content">
              <Heading className='homepage-background__title' as="h1" size="xl" mb={5}>
                { title }
              </Heading>
              <div className="homepage-background__description" dangerouslySetInnerHTML={{__html: content}} />
              <div className="gray_line"></div>
              <div className="homepage-background__quote">
                { homepage_quote.quote.quoteContent }
              </div>
              <div className="homepage-background__author">
                <div className="homepage-background__author--photo">
                  <Img fluid={homepage_quote.quoteAuthor.photo.remoteFile.childImageSharp.fluid} />
                </div>
                <div>
                  <div className="homepage-background__author--name">
                    { homepage_quote.quoteAuthor.fullName }
                  </div>
                  <div className="homepage-background__author--position">
                    { homepage_quote.quoteAuthor.position }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </BackgroundImage>
      )}
    </Layout>
  )
}

export const query = graphql`
  fragment HomeImage on File {
    childImageSharp {
      fluid(maxWidth: 1440, maxHeight: 750) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }
  
  fragment personalInfo on File {
    childImageSharp {
      fluid(maxWidth: 40, maxHeight: 40) {
        ...GatsbyImageSharpFluid_tracedSVG
      }
    }
  }

  query homepage($id: String!) {
    page: wpPage(id: { eq: $id }) {
      title
      content
      homepage_quote {
        quote {
          quoteContent
        }
        quoteAuthor {
          fullName
          photo {
            remoteFile {
              ...personalInfo
            }
          }
          position
        }
      }
      featuredImage {
        node {
          remoteFile {
            ...HomeImage
          }
        }
      }
    }
  }
`
