import React from 'react'
import { graphql, Link, useStaticQuery } from 'gatsby'
import GatsbyLogo from '../../assets/images/Logo.png'

import './header.scss';

const Header = () => {
  const { allWp } = useStaticQuery(graphql`
    {
     allWp {
        edges {
          node {
            generalSettings {
              title
            }
          }
        }
      }
    }
  `)

  return (
    <header id="masthead" className="site-header">
      <Link to="/">
        <img src={GatsbyLogo} alt={allWp.edges[0].node.generalSettings.title} title={allWp.edges[0].node.generalSettings.title}/>
      </Link>
    </header>
  )
}

export default Header