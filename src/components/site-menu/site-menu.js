import React from 'react'
import { Link, useStaticQuery, graphql } from 'gatsby'
import { Menu } from '@chakra-ui/core'
import { normalizePath } from '../../utils/get-url-path'

import './site-menu.scss'

const SiteMenu = () => {
  const { wpMenu } = useStaticQuery(graphql`
    {
      wpMenu(slug: { eq: "main-menu" }) {
        name
        menuItems {
          nodes {
            label
            url
            parentId
            connectedNode {
              node {
                ... on WpContentNode {
                  uri
                }
              }
            }
          }
        }
      }
    }
  `)

  return !!wpMenu && !!wpMenu.menuItems && !!wpMenu.menuItems.nodes ? (

    <Menu>
      <nav className="main-navigation">
        <ul className="primary-menu">
          {wpMenu.menuItems.nodes.map((menuItem, i) => {
            if (menuItem.parentId) {
              return null
            }

            const path = menuItem?.connectedNode?.node?.uri ?? menuItem.url

            return (
              <li key={i + menuItem.url}>
                <Link
                  style={{ display: `block` }}
                  to={normalizePath(path)}
                >
                  {menuItem.label}
                </Link>
              </li>
            )
          })}
        </ul>
      </nav>
    </Menu>

  ) : null
}

export default SiteMenu