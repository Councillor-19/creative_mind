import React from 'react'
import Header from '../header'
import SiteMenu from '../site-menu'

import '../../assets/style.scss'
import './layout.scss'

const Layout = ({ children }) => (
  <div id="page" className="site">
    <nav className="site-sidebar">
      <Header />
      <SiteMenu />
    </nav>

    <main id="main" className="site-main">
      {children}
    </main>

  </div>
)

export default Layout
